#!/usr/bin/python

import math
import numpy as np
import os
import subprocess
from os.path import isfile, join, isdir
#import matplotlib.pyplot as plt
from os import listdir
from PIL import Image


def exportvec(filename,xvec,yvec):

	if len(xvec)==len(yvec):
		fileout=open(filename,"w")	
		for ix, x in enumerate(xvec,0):	
			fileout.write(str(x)+" "+str(yvec[ix])+"\n")
		fileout.close
	else :
		print ("xvec, yvec have different lengths")
		print ("len(xvec)", len(xvec)," len(yvec)", len(yvec))
#	fout.write( str( numlist[inum])+" "+str(abs(fitParams[5])/abs(fitParams[2]))+"\n" )


########################################################3
########################################################


Ndsp=2

# veo si existe el file de "ya hecho"
alreadyfile="alreadyDSP.out"
exists=os.path.isfile(alreadyfile)#.is_file():
 

folder="."

if exists == 1 :
	fread=open(alreadyfile,"r")
	first_line = fread.readline()
	print (first_line)
	print ("First file px :")
	first_line = fread.readline()
	print (first_line)
	fread.close

if exists == 0 :
	fwrite=open(alreadyfile , "w")
	fwrite.write("Downsampling factor: "+str(Ndsp)+"\n")
	fileLs=[f for f in listdir(folder) if not isdir(join(folder, f)) if ".py" not in f if ".out" not in f]
	print (fileLs)
	#pX0,pY0,pX,pY=[],[],[],[]
	for ifile, filete in enumerate(fileLs,0) :
		filete
		#pixX,pixY= Image.open(open(filete)).size
		print (filete)#, pixX, pixY)
#		if pixX >= pixY:
#			XY=" "
#			Npix=pixX/Ndsp
#		else :
#			XY="x"
#			Npix=pixY/Ndsp
#		print ( Npix)
#		cmdstr="convert "+filete+" -resize "+XY+str(Npix)+" "+filete
		convfact=100/Ndsp
		cmdstr="convert "+filete+" -resize "+str(convfact)+"% "+filete
		print(cmdstr)
		os.system(cmdstr)
		
		fwrite.write(str(convfact)+"\n")
## 
		
	fwrite.close

