Image batch downsampler scripts


Regular cellphone photography often produces high resolution images, i.e., images rendered into 12MP or 16MP are practically the norm. However, a number of these resulting images may not truly contain full 12MP or 16MP of information due to low light conditions, shaky hands or other blur-inducing conditions. 

As someone who can return from a trip with 10GB of photos, a lot of times I do not need the 12MP or 16MP resolution, or the pictures themselves may not warrant their pixel count, as stated above. Switching the camera app resolution to lower values back and forth could be a viable idea, but I find it more practical to take all the pictures in the high res, and sorting out later which ones to downsample and which ones to keep as they are.

Therefore, for storage reasons I often apply a downsampling factor of 2 and sometimes 3, leading to a fourfold or ninefold reduction in drive usage. As the number of image files can go up to hundreds or thousands, I scripted the downsampling procedure. Nothing less, nothing more. 

I made two versions. There is a python version that reduces the file *in place*, i.e., replaces the original files with their downsampled versions, and as such reduction is irreversible, the code produces a file, which prevents successive downsampling. The *.sh* version outputs the downsampled files into the *out/* directory, and is safer.

Both scripts rely on the *convert* command from ImageMagick. 



